import java.util.Scanner;

public class Exercitiii {
    protected String facultate;
    //Just a comment
    protected boolean adevaratFals;
    protected String raspuns;

    public void introducereFacultate() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Intrare in program...");
        System.out.print("Introducere facultate (Matematica / Informatica): ");
        String facultate = scanner.nextLine();


        while (!facultate.equalsIgnoreCase("Matematica") && !facultate.equalsIgnoreCase("Informatica")) {
            System.out.print("Optiunea introdusa nu este valida. Introduceți Matematica sau Informatica: ");
            facultate = scanner.nextLine();
        }

        this.facultate = facultate;
        System.out.println("A fost aleasa Facultatea " + facultate + ".");

        System.out.print("Doriti să introduceți studenti noi? (Da/Nu): ");
        raspuns = scanner.nextLine();

        if (raspuns.equalsIgnoreCase("Da")) {
            adevaratFals = true;
            creareGrupaNoua();
        } else if (!raspuns.equalsIgnoreCase("nu")) {
            System.out.print("Optiunea introdusa nu este valida. Introduceți Da sau Nu: ");
            raspuns = scanner.nextLine();
            introducereFacultate();
        }
            else
            System.out.println("Iesire din program...");
        }


    public void creareGrupaNoua() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Creati numele grupei noi:");
        String numeGrupa = scanner.nextLine();

        System.out.println("Numele grupei noi este: " + numeGrupa);


        System.out.println("Doriti să introduceti studenti noi? (Da/Nu):");
        String option = scanner.nextLine();

        if (option.equalsIgnoreCase("Da")) {
            creareGrupaNoua();
        } else {
            adevaratFals = false;
            System.out.println("Programul a fost incheiat.");
        }
    }

    public boolean isContinuareProgram() {
        return adevaratFals;
    }

    public static void main(String[] args) {
        Exercitiii student = new Exercitiii();
        student.introducereFacultate();

        while (student.isContinuareProgram()) {
            student.introducereFacultate();
        }
    }
}

